<?php
/**
 * Plugin Name: Shops Data For Seo
 * Description: Get the data from DFS
 * Version: PoC
 * Author: alexa
 */

// Plugin Base File
define('SDFS_PLUGIN_BASE', __FILE__);

// Plugin Directory Path
define('SDFS_PLUGIN_DIR', untrailingslashit(plugin_dir_path( __FILE__ )));

// Plugin Directory URL
define('SDFS_PLUGIN_URL', untrailingslashit(plugin_dir_url( __FILE__ )));


class SDFSMetabox {

  // singleton
  private static $instance;
  public static function get_instance() {
    if ( null == self::$instance ) {
      self::$instance = new SDFSMetabox();
    } 
    return self::$instance;
  } 

  private function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }

    add_action("wp_ajax_savekws", array( $this, 'save_kws' ));

  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    // add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

    add_action( 'admin_enqueue_scripts', array( $this, 'load_css' ));
    add_action( 'admin_enqueue_scripts', array( $this, 'load_js'  ));
      
  }

  public function load_css($hook) 
  {
    if ($hook != 'edit.php')
      wp_enqueue_style('shops-dfs-css', SDFS_PLUGIN_URL . '/css/shops-dfs.css' );
  }

  public function load_js($hook) 
  {
        if (!is_admin()) return;
        wp_enqueue_script('shops-dfs-js', SDFS_PLUGIN_URL . '/js/shops-dfs.js', array('jquery'), false, true);
        // make some data available to js
        $wp_urls = array( 'admin_fold' => admin_url() );
        wp_localize_script( 'shops-dfs-js', 'wp_urls', $wp_urls );
    /*if ($hook != 'edit.php') {
      wp_enqueue_script( 'admin-relevance-metabox-js',  SDFS_PLUGIN_URL . '/assets/js/admin/metabox.js' );
    }*/
  }

  public function add_metabox() 
  {
    add_meta_box( 'sdfs_data', 'Shops Data For Seo', array( $this, 'render_metabox' ), 'shop', 'side', 'core');
  }

  public function render_metabox( $post ) {

    // Add nonce for security and authentication.
    wp_nonce_field( 'sdfs_nonce_action', 'sdfs_nonce' );

    // Retrieve an existing value from the database.
   

    // Load data from database if any
    $dfs_kws = get_post_meta( $post->ID, 'dfs_kws', false);


    // dummy example

   /* $dates_dummy = ['2020-12-05', '2020-05-21', '2019-05-30'];
    $kws_d = [
      '2020-12-05' => ['Jedan','Dva','Tri'], 
      '2020-05-21' => ['Crveno', 'Plavo', 'Belo'], 
      '2019-05-30' => ['Jabuka', 'Grozdje']
    ];*/


    ?>

    <h4>Keyword searches by date</h4>

    <?php
    /*<select id="date_choice" name="date_choice">
      <?php
        foreach ($kws_d as $date_key => $keywords_dummy) {
          echo '<option value="'.$date_key.'"'.selected(end($kws_d) == $keywords_dummy).'>'.$date_key.'</option>';
        }
      ?>
    </select>
    
    <div class="kws-results">
      <?php
        foreach ($kws_d as $date_key => $keywords_dummy) {
          echo '<ul id="'.$date_key.'" class="kw-dates">';
            foreach ($keywords_dummy as $kw) {
              echo '<li>'.$kw.'</li>';
            }
          echo '</ul>';
        }
      ?>
    </div>*/
    ?>

    <div class="new-kw-result">
      <h4>New Result</h4>
      <div class="new-kws-result-data"></div>
    </div>

    <input class="request-new-kw button button-primary button-large" id="_btn" type="button" value="Request new keywords" onclick="requestKw(<?= $post->ID ?>)" />

    <script type="text/javascript">
      // show / hide results
      jQuery( document ).ready(function() {
          // here is last $date_key from the php array
          jQuery('#' + "<?=$date_key?>").show();
          jQuery('#date_choice').change(function(){
              jQuery('.kw-dates').hide();
              jQuery('#' + jQuery(this).val()).show();
          });
      });
    </script>

    <?php
  }

  // public function save_metabox( $post_id, $post ) {}

  public function save_kws() {

    $vlists = get_terms( 
                array( 
                  'taxonomy' => 'voucher_list', 
                  'includes' => implode(',', get_post_meta($_REQUEST['post_id'], 'voucher_list_id', false)), 
                  // 'fileds' => 'all_with_object_id',
                  'hide_empty' => false 
                ));
    $vl_names = '';

    foreach ($vlists as $vlist)
      $vl_names .= $vlist->name.',';

    // vars to send request to API
    $shop_id = $_REQUEST['post_id'];
    $shop_name = get_the_title($_REQUEST['post_id']);
    $vl_names = rtrim($vl_names, ','); // finally ok
    $sent_kws = ''; // array kw to send
    $project = str_replace('www.', '', parse_url( get_site_url() )['host']);
    $reverse = false;

    $data = [
      'shop_id' => $shop_id,
      'shop_name' => $shop_name, 
      'v_lists' => $vl_names, 
      'kws' => '', 
      'project' => $project, 
      'reverse' => $reverse
    ];

    // create call to API endpoint
    $api_response = $this->call_tool_api('http://test1.wettbewerbe365.ch/dataforseo/wp/kw_controller.php', $data);

    // save new data if any

    // return to shop

    // echo "test vracanja"; -> works

    //die($_REQUEST['post_id']);

    // $today = date("Y-m-d");

    // dummy example:
    // $kws_set = ['Aleksa' => 10, 'Stefan' => 20, 'Katarina' => 30];

    // json_encode($today);

    die($api_response);

  }

  public function call_tool_api($url, $data = false)
  {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_POST, 1);
      if($data)
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      
      //  $url = sprintf("%s?%s", $url, http_build_query($data));

      // Optional Authentication:
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_USERPWD, "uvoz:stekatale");

      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($curl);

      curl_close($curl);

      return $result;
  }

}

add_action( 'plugins_loaded', array( 'SDFSMetabox', 'get_instance' ) );


?>